<?php

namespace App\Repository;

use App\Entity\NonUser;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method NonUser|null find($id, $lockMode = null, $lockVersion = null)
 * @method NonUser|null findOneBy(array $criteria, array $orderBy = null)
 * @method NonUser[]    findAll()
 * @method NonUser[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class NonUserRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, NonUser::class);
    }

    // /**
    //  * @return NonUser[] Returns an array of NonUser objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('n')
            ->andWhere('n.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('n.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?NonUser
    {
        return $this->createQueryBuilder('n')
            ->andWhere('n.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
